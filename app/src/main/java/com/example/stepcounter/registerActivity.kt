package com.example.stepcounter

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*
import com.google.firebase.database.DatabaseReference



class registerActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance().reference
        init()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
    }

    fun init() {




        signUpButton.setOnClickListener {
            if (passwordEditText.text.toString().isNotEmpty() && confirmPasswordEditText.text.toString().isNotEmpty() && passwordEditText.text.toString().equals(
                    confirmPasswordEditText.text.toString()
                )
            ) {
                auth.createUserWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Jemala", "createUserWithEmail:success")
                            Toast.makeText(this,"Registration Success",Toast.LENGTH_LONG).show()

                            var email = emailEditText.text.toString()
                            val re1 = Regex("[.]")
                            val re2 = Regex("[@]")
                            email = re1.replace(email,"დ")
                            email = re2.replace(email,"ე")

                            database.child("$email").setValue("0")

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Jemala", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        // ...
                    }
            }
            else{
                Toast.makeText(this,"Passwords Doesn't Match",Toast.LENGTH_LONG).show()
            }
        }
    }
}
