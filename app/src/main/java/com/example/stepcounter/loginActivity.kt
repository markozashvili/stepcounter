package com.example.stepcounter

import android.app.PendingIntent.getActivity
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.core.Context
import android.content.Context.SENSOR_SERVICE
import android.support.v4.content.ContextCompat.getSystemService
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_signin_auth.*


class loginActivity() : AppCompatActivity(), SensorEventListener{
    private lateinit var database: DatabaseReference
    private lateinit var auth: FirebaseAuth

    var running = false
    var sensorManager: SensorManager? = null
    var signInAuth = getIntent()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        database = FirebaseDatabase.getInstance().reference
        this.sensorManager = getSystemService(android.content.Context.SENSOR_SERVICE) as SensorManager
    }

    override fun onResume() {
        super.onResume()
        running = true
        var stepSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        if (stepSensor == null) {
            Toast.makeText(this, "No Step Counter Sensor", Toast.LENGTH_LONG).show()
        } else {
            sensorManager?.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onPause() {
        super.onPause()
        running = false
        sensorManager?.unregisterListener(this)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val database = FirebaseDatabase.getInstance()
        var mail = signInAuth.getStringExtra("email")

        if (running) {
            if (event != null) {
                stepsValue.text = event.values[0].toString()
                val re1 = Regex("[.]")
                val re2 = Regex("[@]")
                mail = re1.replace(mail,"დ")
                mail = re2.replace(mail,"ე")
                val myRef = database.getReference("$mail")

                myRef.addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        val value = dataSnapshot.getValue(String::class.java)
                        Log.d("Jemala", "Value is: $value")
                        if (value != null) {
                            totalStepsValue.text = (value.toInt() + event.values[0]).toString()
                        }

                        val database = FirebaseDatabase.getInstance()
                        val myRef = database.getReference("$mail")

                        if (value != null) {
                            myRef.setValue("${value.toInt() + event.values[0]}")
                        }
                    }

                    override fun onCancelled(error: DatabaseError) {
                        // Failed to read value
                        Log.w("Jemala", "Failed to read value.", error.toException())
                    }
                })

            }
        }

    }
}
