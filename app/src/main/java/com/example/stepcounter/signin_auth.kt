package com.example.stepcounter

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin_auth.*

class signin_auth : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    var email = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin_auth)
        auth = FirebaseAuth.getInstance()
        init()
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
    }

    fun init() {
        signInButton.setOnClickListener {
            email = emailauthEditText.text.toString()
            auth.signInWithEmailAndPassword(email, passwordauthEditText.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("Jemala", "signInWithEmail:success")
                        val user = auth.currentUser
                        var intent = Intent(this, loginActivity::class.java)
                        intent.putExtra("email",email)
                        startActivity(intent)
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("Jemala", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    // ...
                }
        }


    }
}
